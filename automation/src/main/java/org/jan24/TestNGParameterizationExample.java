package org.jan24;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNGParameterizationExample {

	WebDriver driver;

	@Parameters({ "browser_Name", "os" })
	@Test
	public void setBrowser(@Optional("firefox") String browserName, String operatingSystem) {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");

		System.out.println("Browser Name : " + browserName);
		System.out.println("OS : " + operatingSystem);

		if (browserName.equalsIgnoreCase("chrome")) {

			driver = new ChromeDriver();

		} else {

			System.out.println("Invalid Browser ");
		}

	}

}

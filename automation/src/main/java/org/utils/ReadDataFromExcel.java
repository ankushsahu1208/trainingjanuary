package org.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ReadDataFromExcel {

	public static void main(String[] args) throws IOException {
		
		FileInputStream fis=new FileInputStream(new File("C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\Book1.xlsx"));
		
		Workbook wb= new XSSFWorkbook(fis);
		
		Sheet sheet= wb.getSheetAt(0);
		
		/** Sheet sheet= wb.getSheet("Sheet Name"); */
		
		Row firstRow=sheet.getRow(0);
		
		Cell firstCell=firstRow.getCell(0);
		
		Cell secondCell=firstRow.getCell(1);
		
		/** Cell thirdCell=firstRow.getCell(2); */ //java.lang.NullPointerException
		
		System.out.println(firstCell.getStringCellValue());
		
		System.out.println(secondCell.getStringCellValue());
		
		/** System.out.println(thirdCell.getStringCellValue()); */
		
		wb.close();	
	}
}

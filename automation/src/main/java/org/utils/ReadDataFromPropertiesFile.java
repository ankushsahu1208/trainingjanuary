package org.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ReadDataFromPropertiesFile {

	public static String configuration(String key) throws IOException {
		
		FileInputStream fis= new FileInputStream("C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\Configuration.properties");
		
		Properties prop= new Properties();
		
		prop.load(fis);
		
		System.out.println(prop.getProperty("url"));
		return prop.getProperty(key);
	}
	
	
	public static void main(String[] args) throws IOException {
		
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(configuration("url"));

	}
	
	
}

package org.jan24;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AdvancedXpaths {

	public static void main(String[] args) {
		
		WebDriver driver= new ChromeDriver();
		
		driver.findElement(By.xpath("//input[@type='image']"));
		driver.findElement(By.xpath("//input[@type=\"image\"]"));	
		
	}
}

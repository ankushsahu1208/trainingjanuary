package org.jan30;

import org.testng.annotations.Test;

public class TestNGGroupingExample {

	@Test(priority = 1, groups = { "Sanity" })
	public void A1() {

		System.out.println("sanity A1");
	}

	@Test(priority = 2, groups = { "Sanity" })
	public void A2() {

		System.out.println("sanity A2");
	}

	@Test(priority = 3, groups = { "Regression" })
	public void A3() {

		System.out.println("Regression A2");
	}

	@Test(priority = 4, groups = { "Regression" })
	public void A4() {

		System.out.println("Regression A2");
	}

	@Test(priority = 5, groups = { "Regression" })
	public void A5() {

		System.out.println("Regression A2");
	}

	@Test(priority = 6, groups = { "Regression" })
	public void A6() {

		System.out.println("Regression A2");
	}

	@Test(priority = 7, groups = { "Regression" })
	public void A7() {

		System.out.println("Regressin A2");
	}

}

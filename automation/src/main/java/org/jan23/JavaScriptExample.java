package org.jan23;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.utils.ReusableMethods;

public class JavaScriptExample {

	
	@Test
	public void TestJavaScript() throws IOException {
		
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("javaScriptUrl"));
		
		JavascriptExecutor jse=(JavascriptExecutor)driver;
		
		jse.executeScript(("window.scrollTo(0,1000)"));
		
		
	}
	
}

package org.jan20;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 
 * 
 * Steps
 * 
 * 1) Excel Sheet Path(find excel sheet ) 2) Open Excel Sheet 3) Open the
 * sheet(first or second) 4) selecting the Row 5) row--Selecting the Cell 6)
 * fetching the cell's value
 *
 */

public class ReadDataFromExcelSheet {

	public static void main(String[] args) throws IOException {

		/** Step 1 */

		File file = new File("C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\Book1.xlsx");

		FileInputStream fis = new FileInputStream(file);

		/** For xlsx =XSSFWorkBook class can be used */
		/** For xls= HSSFWorkBook class be used */

		/** Step 2 */

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		/** Step 3 */

		// XSSFSheet sheet=workbook.getSheet("Sheet1");

		XSSFSheet sheet = workbook.getSheetAt(0);

		/** Step 4 */

		XSSFRow row = sheet.getRow(0);

		/** Step 5 */

		XSSFCell firstCell = row.getCell(0);
		
		XSSFCell secondCell= row.getCell(1);
		
		//XSSFCell thirdCell= row.getCell(2); //Null Pointer exception

		/** Step 6 */

		System.out.println(firstCell.getStringCellValue());

		System.out.println(secondCell.getStringCellValue());
		
		//System.out.println(thirdCell.getStringCellValue());
	}

}

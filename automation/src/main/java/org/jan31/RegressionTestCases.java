package org.jan31;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.utils.ReusableMethods;

public class RegressionTestCases extends TestBase {

	@Test(priority = 1, enabled = true)
	public void verifyTitle() throws IOException {

		String expectedTitle = "Welcome: Mercury tours";

		driver.get(ReusableMethods.getConfiguration("url"));

		String actualTitle = driver.getTitle();

		Assert.assertEquals(actualTitle, expectedTitle);

		Assert.assertEquals(actualTitle, expectedTitle, "Application unmatched");
	}

	@Test(priority = 2, enabled = true, dependsOnMethods = "verifyTitle")
	public void verifyLogin() {

		By userNameLocator = By.name("userName");

		waitForPresenceOfElementLocated(userNameLocator);

		WebElement userNameTextBBox = driver.findElement(userNameLocator);

		waitForvisibilityOf(userNameTextBBox);

		userNameTextBBox.sendKeys("Ankush");

		// -------------------------------------------------------------------

		By passwordLocator = By.name("password");

		waitForPresenceOfElementLocated(passwordLocator);

		WebElement passwordTextBox = driver.findElement(passwordLocator);

		waitForvisibilityOf(passwordTextBox);

		passwordTextBox.sendKeys("Ankush123");
		
		// -------------------------------------------------------------------

	}

}

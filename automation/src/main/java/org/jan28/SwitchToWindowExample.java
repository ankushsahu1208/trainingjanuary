package org.jan28;

import java.util.Set;
import org.jan27.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SwitchToWindowExample extends BaseTest {

	@Test
	public void test1() throws InterruptedException {

		driver.get("https://www.w3schools.com/");

		String parentWindow = driver.getWindowHandle();

		System.out.println(parentWindow);

		// ---------------------------------------------------------------------------

		WebDriverWait wait = new WebDriverWait(driver, 20);

		WebElement tryItYourSelf = driver.findElement(By.xpath("(//a[text()='Try it Yourself »'])[1]"));

		wait.until(ExpectedConditions.visibilityOf(tryItYourSelf));

		tryItYourSelf.click();

		// -----------------------------------------------------------------------------

		Set<String> windows = driver.getWindowHandles();

		/*
		 * for (String s : windows) {
		 * 
		 * System.out.println(s);
		 * 
		 * }
		 */

		/*
		 * Iterator<String> itr=windows.iterator();
		 * 
		 * while(itr.hasNext()) {
		 * 
		 * 
		 * System.out.println(itr.next()); }
		 */

		System.out.println("--------------------------");

		windows.remove(parentWindow);

		String childWindow = windows.iterator().next();

		driver.switchTo().window(childWindow);

		driver.switchTo().frame("iframeResult");

		Thread.sleep(2000);

		System.out.println(driver.findElement(By.xpath("//h1[contains(text(),'This is a Heading')]")).getText());
		
		Assert.assertEquals(driver.findElement(By.xpath("//h1[contains(text(),'This is a Heading')]")).getText(), "This is a Heading");

		Thread.sleep(2000);
		
		driver.switchTo().window(parentWindow);
		
		System.err.println(driver.findElement(By.xpath("//p[contains(text(),'The language for building web pages')]")).getText());
	}
}

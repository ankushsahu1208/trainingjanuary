package org.jan22;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.jan21.ReadDataFromExcel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.utils.ReusableMethods;

public class TestNGDemo1 {

	@Test(priority=1,enabled=false)
	public void ValidateLoginFunctionality() throws IOException, InterruptedException {
		
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("ursl"));
		
		driver.findElement(By.name("userName")).sendKeys(ReadDataFromExcel.getExcelData(ReusableMethods.getConfiguration("SheetName"), 0, 0));

		driver.findElement(By.name("password")).sendKeys(ReadDataFromExcel.getExcelData(ReusableMethods.getConfiguration("SheetName"), 0, 1));
		
		Thread.sleep(5000);
		
		driver.close();

	}
	
	@Test(priority=2,enabled=true,description="Validating the Registration Functionality")
	public void ValidateRegistration() {
		
		System.out.println("m2 hello");
	}
	
	@Test(priority=3,enabled=true)
	public void ValidateTitle() {
		
		System.out.println("m2 hello");
	}
	
	
}

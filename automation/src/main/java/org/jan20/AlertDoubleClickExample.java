package org.jan20;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AlertDoubleClickExample {

	
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.testandquiz.com/selenium/testing.html");

		WebElement element=driver.findElement(By.xpath("//*[@id=\"dblClkBtn\"]"));
		
		/** Actions class for double click */
		
		
		Actions action= new Actions(driver);
		
		action.doubleClick(element).build().perform();
		
		/** Alert will appear */
		
		Alert alert=driver.switchTo().alert();
		
		String alertText=alert.getText();
		
		System.out.println("Alert Text : "+alertText);
		
		alert.accept();
		
		Thread.sleep(5000);
		
		driver.close();
		
		
	}
	
	
}

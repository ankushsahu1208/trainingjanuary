package org.jan30;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class ExcelSheetData {

	public static void main(int[] args) throws IOException {

		FileInputStream fis = new FileInputStream(new File(System.getProperty("user.dir") + "\\ExcelData.xlsx"));

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Credentials");

		String firstRowFirstCell = sheet.getRow(0).getCell(0).getStringCellValue();

		System.out.println(firstRowFirstCell);

		String secondRowSecondCell = sheet.getRow(0).getCell(1).getStringCellValue();

		System.out.println(secondRowSecondCell);

		workbook.close();

	}

	@DataProvider(name = "testData")
	public static Object[][] readUserNameAndPwd() throws IOException {

		FileInputStream fis = new FileInputStream(new File(System.getProperty("user.dir") + "\\ExcelData.xlsx"));

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Credentials");

		// --------------------------------------------------------

		int lastRowNumber = sheet.getLastRowNum();

		int lastCellNumber = sheet.getRow(lastRowNumber).getLastCellNum();

		System.out.println(lastRowNumber + "  " + lastCellNumber);

		Object[][] obj = new Object[lastRowNumber + 1][lastCellNumber];

		// ---------------------------------------------------------

		for (int i = 0; i <= lastRowNumber; i++) {

			for (int j = 0; j < lastCellNumber; j++) {

				obj[i][j] = sheet.getRow(i).getCell(j).getStringCellValue();

				System.out.println(obj[i][j]);

			}
		}
		workbook.close();

		return obj;

	}

}

package org.jan30;

import org.testng.annotations.Test;

public class TestNGGroupingExample2 {

	@Test(priority = 1, groups = { "Sanity" })
	public void A1() {

		System.out.println("sanity A1");
	}

	@Test(priority = 2, groups = { "Sanity" })
	public void A2() {

		System.out.println("sanity A2");
	}

	@Test(priority = 30, groups = { "Regression","Sanity" })
	public void A23() {

		System.out.println("Regression A2");
	}

	@Test(priority = 40, groups = { "Regression" })
	public void A24() {

		System.out.println("Regression A2");
	}

	@Test(priority = 50, groups = { "Regression" })
	public void A25() {

		System.out.println("Regression A2");
	}

	@Test(priority = 60, groups = { "Regression" })
	public void A26() {

		System.out.println("Regression A2");
	}

	@Test(priority = 70, groups = { "Regression" })
	public void A27() {

		System.out.println("Regressin A2");
	}

}

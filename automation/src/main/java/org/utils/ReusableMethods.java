package org.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReusableMethods {

	public static String getConfiguration(String key) throws IOException {

		FileInputStream fis = new FileInputStream(new File(System.getProperty("user.dir") + "\\Configuration.properties"));

		Properties prop = new Properties();

		prop.load(fis);

		//System.out.println(prop.get("url"));
		
		System.out.println(prop.getProperty(key));
		 
		return prop.getProperty(key);
		
	}

	

}

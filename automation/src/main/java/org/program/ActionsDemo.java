package org.program;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.utils.ReusableMethods;

public class ActionsDemo {

	public static void main(String[] args) throws IOException {

		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("url"));
		
		/** Actions Class */
		
		Actions action= new Actions(driver);
		
		/**-------------------*/
		
		driver.findElement(By.name("userName")).sendKeys(ReusableMethods.getConfiguration("userID"));

		driver.findElement(By.name("password")).sendKeys(ReusableMethods.getConfiguration("pwd"));

		//driver.findElement(By.name("login")).click();
		
		//action.click(driver.findElement(By.name("login"))).build().perform();
		
		/*Actions a1=action.click(driver.findElement(By.name("login")));
		Action  a=a1.build();
		a.perform();*/
		
		Action  builder=action.click(driver.findElement(By.name("login"))).build();
		
		builder.perform();
		

		action.moveToElement(driver.findElement(By.name("login"))).click().build().perform();;

		
		
	}
}

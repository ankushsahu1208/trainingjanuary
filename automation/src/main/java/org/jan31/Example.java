package org.jan31;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Example {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		driver.manage().deleteAllCookies();

		driver.manage().window().maximize();

		driver.get("https://www.facebook.com/");

		By firstName = By.name("firstname");

		System.out.println(firstName);

		WebElement element = driver.findElement(firstName);

		System.out.println(element);
		
		By reg_email = By.name("reg_email__");

		WebElement elementlast = driver.findElement(reg_email);

		System.out.println(elementlast);
		
	}
}

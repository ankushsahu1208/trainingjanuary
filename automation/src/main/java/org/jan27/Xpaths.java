package org.jan27;

public class Xpaths {

	/*
	 * Absolute XPath : /
	 * 
	 * Relative Xpath : //
	 */

	// TagName[@attributeName='value']

	// input[@name='userName']

	// input[@size='10']

	// input[@size="10"]

	// (//input[@size='10'])[1]

	// input[@size='10' and @name='userName']

	// input[@size='10' or @name='userName']

	// *[@size='10']
	// ---------------------------------------------------------------------------------------------------------------------------------
	// TagName[starts-with(@attribute,'value')]

	// TagName[@name='userName'and starts-with(@name,'userName')]

	// input[starts-with(@name,'userName')]

	// *[@name='userName'and starts-with(@name,'userName') and @size='10' and
	// @type='text']

	// ---------------------------------------------------------------------------------------------------------------------------------

	// a[text()='REGISTER']

	// a[contains(text(),'REGISTER')]

	// input[contains(@name,'Name')]

	/*
	 * driver.findElement(By.id("ID")); By.name("Name"); By.linkText("LinkText");
	 * By.cssSelector("Css"); By.xpath("xpath"); By.partialLinkText("")
	 * By.tagName(""); By.className("")
	 */

	// span[text()='Sign up']

	// a[@data-testid="signupButton"]

	// a[@data-testid="signupButton"]/div/span/span

	// a[@data-testid="signupButton"]/child::div

	// a[@data-testid="signupButton"]/child::div/span/span

	// (//div[@dir='auto'])[3]

	// a[@href='/i/flow/signup']

}

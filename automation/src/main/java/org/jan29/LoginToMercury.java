package org.jan29;

import org.jan27.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginToMercury extends BaseTest {

	@DataProvider(name = "testData")
	public Object[][] getusernameAndPassword() {

		Object[][] obj = new Object[2][2];/** new Object[Row][column] */
		obj[0][0] = "Ankush";
		obj[0][1] = "password123";

		obj[1][0] = "sourabh";
		obj[1][1] = "pwdsourabh";
		return obj;
	}

	/** Number of iteration=Number of Rows */

	@Test(description = "Login to Mercury Tours Application", priority = 1, enabled = true, dataProvider = "testData")
	public void login(String user, String pwd) {

		driver.get("http://newtours.demoaut.com/");

		WebElement userName = driver.findElement(By.name("userName"));

		waitForVisibilityOfElement(userName);

		userName.sendKeys(user);

		WebElement password = driver.findElement(By.name("password"));

		waitForVisibilityOfElement(password);

		password.sendKeys(pwd);

		WebElement loginBtn = driver.findElement(By.name("login"));

		loginBtn.click();

	}

}

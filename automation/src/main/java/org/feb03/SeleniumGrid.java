package org.feb03;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.graphbuilder.struc.LinkedList.Node;

public class SeleniumGrid {

	
	
	public static void main(int[] args) {
	
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
		
		ChromeOptions options=new ChromeOptions();
		
		options.addArguments("--headless");

		WebDriver driver = new ChromeDriver(options);
		
		driver.quit();
		
	}
	
	
	
	static String clientUrl;
	
	public static void main(String[] args) throws MalformedURLException {
		
		clientUrl="http://192.168.1.207:4444/wd/hub";
		
		DesiredCapabilities cap= DesiredCapabilities.chrome();
		
		cap.setPlatform(Platform.WIN10);
		
		WebDriver driver= new RemoteWebDriver(new URL(clientUrl), cap);
		
		driver.manage().window().maximize();
		
		driver.get("https://www.reddit.com/");
		
	}
	
	
}

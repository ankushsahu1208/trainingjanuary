package org.jan30;

import org.jan27.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class LoginToMercury extends BaseTest {

	/** Number of iteration=Number of Rows */

	@Test(description = "Login to Mercury Tours Application", priority = 1, enabled = true, dataProvider = "testData",dataProviderClass=ExcelSheetData.class)
	public void login(String user, String pwd) {

		driver.get("http://newtours.demoaut.com/");

		WebElement userName = driver.findElement(By.name("userName"));

		waitForVisibilityOfElement(userName);

		userName.sendKeys(user);

		WebElement password = driver.findElement(By.name("password"));

		waitForVisibilityOfElement(password);

		password.sendKeys(pwd);

		WebElement loginBtn = driver.findElement(By.name("login"));

		loginBtn.click();

	}

}

package org.feb04;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseGrid {

	WebDriver driver;
	String clientURL;

	/*
	 * @BeforeTest public void setUp() throws MalformedURLException {
	 * 
	 * clientURL = "http://192.168.1.207:4444/wd/hub";
	 * 
	 * DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	 * 
	 * capabilities.setPlatform(Platform.WIN10);
	 * 
	 * driver = new RemoteWebDriver(new URL(clientURL), capabilities);
	 * 
	 * }
	 */

	@BeforeTest
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

		driver.manage().deleteAllCookies();

		driver.manage().window().maximize();

	}

	@AfterMethod
	public void screenShot() throws IOException {

		TakesScreenshot tse = (TakesScreenshot) driver;

		File screenShot = tse.getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(screenShot,
				new File("C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\ScreenShots\\firstScreenShot.png"));

	}

	@AfterTest
	public void kill() {

		driver.quit();
	}

}

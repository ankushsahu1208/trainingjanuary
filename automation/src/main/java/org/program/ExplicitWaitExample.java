package org.program;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.utils.ReusableMethods;

public class ExplicitWaitExample {

	public static void main(String[] args) throws IOException, InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://newtours.demoaut.com");

		/** --------------------------------------------- */

		WebElement userNameElement = driver.findElement(By.name("userName"));

		WebDriverWait wait = new WebDriverWait(driver, 60);

		wait.until(ExpectedConditions.visibilityOf(userNameElement));

		userNameElement.sendKeys("ankush");

		/** --------------------------------------------- */

		WebElement passwordElement = driver.findElement(By.name("password"));

		wait.until(ExpectedConditions.visibilityOf(passwordElement));

		passwordElement.sendKeys("password123");

		/** --------------------------------------------- */
		WebElement loginElement = driver.findElement(By.name("login"));

		wait.until(ExpectedConditions.elementToBeClickable(loginElement));

		loginElement.click();

		Thread.sleep(3000);

	}

}

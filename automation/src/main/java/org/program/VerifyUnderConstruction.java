package org.program;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class VerifyUnderConstruction {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();	

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://newtours.demoaut.com/");

		/** driver.navigate().to("http://newtours.demoaut.com/"); */
		/** driver.navigate().back(); */
		
		
		String expected = "Under Construction: Mercury Tours";

		java.util.List<WebElement> elements = driver.findElements(By.tagName("a"));

		int size = elements.size();

		for (int i = 0; i < size; i++) {

			WebElement element = elements.get(i); // Problem solve kro
			
			element.click();
			Thread.sleep(3000);

			if (expected.equals(driver.getTitle())) {

				System.out.println(element.getText());
			} else {
				System.out.println("Not Found");
			}
			
			

		}

		driver.close();
	}

}

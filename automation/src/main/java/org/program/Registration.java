package org.program;

import java.util.Scanner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Registration {

	//static WebDriver driver;

	public static void main(String[] args) {

		System.out.println("Enter the browser name chrome/firefox");
		Scanner sc = new Scanner(System.in);
		
		String browser = sc.nextLine();
		
		//sc.close();

		if (browser.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
			WebDriver driver= new ChromeDriver();
			
		} else if (browser.equalsIgnoreCase("firefox")) {
			
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");
			WebDriver driver= new FirefoxDriver();
		} else {

			System.out.println("Invalid Browser");
		}

	}
}

package org.program;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.utils.ReusableMethods;
public class ActionsExample2 {

	public static void main(int[] args) throws IOException, InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("register"));

		/** Elements */

		WebElement firstName = driver.findElement(By.name("firstName"));

		WebElement lastName = driver.findElement(By.name("lastName"));

		/** Actions methods use -----Example 1 */

		Actions build = new Actions(driver);

		Action firstAction = build.sendKeys(firstName, "Ankush").build();

		firstAction.perform();

		// -------------

		Action secondAction = build.sendKeys(lastName, "Sahu").build();

		secondAction.perform();

		Thread.sleep(5000);

	}

	public static void main(double[] args) throws IOException, InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("register"));

		/** Elements */

		WebElement firstName = driver.findElement(By.name("firstName"));

		WebElement lastName = driver.findElement(By.name("lastName"));

		/** Actions methods use -----Example 2 */

		Actions build = new Actions(driver);

		Action actions = build.sendKeys(firstName, "Ankush").sendKeys(lastName, "Sahu").build();

		actions.perform();

		Thread.sleep(5000);

	}

	public static void main(String[] args) throws IOException, InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("register"));

		/** Elements */

		WebElement firstName = driver.findElement(By.name("firstName"));

		WebElement lastName = driver.findElement(By.name("lastName"));

		/** Actions methods use -----Example 3 */

		Actions build = new Actions(driver);

		//Action actions = build.sendKeys(firstName, "Ankush").sendKeys(lastName, "Sahu").build();
		
		//Action actions = build.contextClick().sendKeys(firstName, "Ankush").sendKeys(lastName, "Sahu").build();
		
		Action actions = build.contextClick(firstName).sendKeys(firstName, "Ankush").sendKeys(lastName, "Sahu").build();
		
		actions.perform();

		Thread.sleep(5000);

	}

}

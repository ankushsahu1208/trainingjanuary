package org.inheritence;

public class Test {
	
	
	
	public static void main(String[] args) {
		
		Utility obj= new Utility();
		//Calling methods by class object
		obj.getConfiguration();
		obj.getConfiguration("Ankush");
		
		
		//Calling method by Class name
		Utility.getConfiguration();
		Utility.getConfiguration("AnkushStatic");
	}
	
}

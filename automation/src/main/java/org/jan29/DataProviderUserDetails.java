package org.jan29;

import org.testng.annotations.DataProvider;

public class DataProviderUserDetails {

	@DataProvider(name = "testData")
	public static Object[][] getusernameAndPassword() {

		Object[][] obj = new Object[2][2];
		obj[0][0] = "Ankush";
		obj[0][1] = "password123";

		obj[1][0] = "sourabh";
		obj[1][1] = "pwdsourabh";
		return obj;
	}

}

package org.program;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.utils.ReusableMethods;

public class LoginUsingPropertiesFile {

	public static void main(String[] args) throws IOException, InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("url"));

		driver.findElement(By.name("userName")).sendKeys(ReusableMethods.getConfiguration("userID"));

		driver.findElement(By.name("password")).sendKeys(ReusableMethods.getConfiguration("pwd"));

		driver.findElement(By.name("login")).click();

		Thread.sleep(3000);

	}

}

package org.jan27;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

	public WebDriver driver;
	
	public WebDriverWait wait;

	public void waitForVisibilityOfElement(WebElement element) {
		
		wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.visibilityOf(element));

	}

	@BeforeMethod
	public void setUp() {

		System.out.println("Start Execution");

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

		driver.manage().deleteAllCookies();

		driver.manage().window().maximize();

	}

	@AfterMethod
	public void quit() {

		driver.quit();

		System.out.println("End Execution");
	}
}

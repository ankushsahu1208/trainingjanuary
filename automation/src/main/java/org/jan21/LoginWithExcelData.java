package org.jan21;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.utils.ReusableMethods;

public class LoginWithExcelData {

public static void main(String[] args) throws IOException, InterruptedException {

		
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(ReusableMethods.getConfiguration("url"));
		
		driver.findElement(By.name("userName")).sendKeys(ReadDataFromExcel.getExcelData(ReusableMethods.getConfiguration("SheetName"), 0, 0));

		driver.findElement(By.name("password")).sendKeys(ReadDataFromExcel.getExcelData(ReusableMethods.getConfiguration("SheetName"), 0, 1));
		
		Thread.sleep(5000);
		
		driver.close();
}

}

package org.feb05;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {

	@FindBy(name = "email")
	WebElement emailTextBox;

	@FindBy(how = How.ID, using = "pass")
	WebElement passwordTextBox;

	public void setEmailTextBox(String email) {

		emailTextBox.sendKeys(email);
	}

	public void setPasswordTextBox(String password) {

		passwordTextBox.sendKeys(password);
	}

}

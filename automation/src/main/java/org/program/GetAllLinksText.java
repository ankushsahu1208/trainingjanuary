package org.program;

import java.awt.List;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GetAllLinksText {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\Desktop\\SSITraining\\automation\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://newtours.demoaut.com/");

		java.util.List<WebElement> elements = driver.findElements(By.tagName("a"));

		int size = elements.size();

		for (int i = 0; i < size; i++) {

			String elementText = elements.get(i).getText();
			System.out.println(elementText);
		}

		driver.close();

	}
}

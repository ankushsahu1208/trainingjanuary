package org.jan27;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class SignUpTwitterExample extends BaseTest {

	@Test
	public void signup() throws InterruptedException {

		System.out.println("Singup");

		driver.navigate().to("https://twitter.com/?lang=en");

		driver.findElement(By.xpath("//a[@data-testid='signupButton']")).click();

		WebElement userName = driver.findElement(By.xpath("//input[@name='name']"));

		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.visibilityOf(userName));

		userName.sendKeys("Ankush");

		driver.findElement(By.xpath("//input[@name='phone_number']")).sendKeys("9999999999");

		driver.findElement(By.xpath("//span[text()='Next']")).click();

		Thread.sleep(5000);

	}

}

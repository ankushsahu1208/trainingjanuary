package org.program;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlertExample {

	/** Alert Example */

	public static void main(double[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.testandquiz.com/selenium/testing.html");

		driver.findElement(By.xpath("/html/body/div/div[11]/div/p/button")).click();

		Alert alert = driver.switchTo().alert();

		alert.accept();

		Thread.sleep(50000);

	}

	/** ExpectedCondition titleIs("") */

	public static void main(int[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.testandquiz.com/selenium/testing.html");

		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.titleIs("Sample Test Page1"));

		driver.findElement(By.xpath("/html/body/div/div[11]/div/p/button")).click();

		Alert alert = driver.switchTo().alert();

		alert.accept();

		Thread.sleep(5000);

		driver.close();

		/**
		 * 
		 * Exception in thread "main" org.openqa.selenium.TimeoutException: Expected
		 * condition failed: waiting for title to be "Sample Test Page1". Current title:
		 * "Sample Test Page" (tried for 30 second(s) with 500 milliseconds interval)
		 */

	}
	
	
	
	/** Double Click -Actions class */

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.testandquiz.com/selenium/testing.html");

		WebElement doubleClickElement =driver.findElement(By.xpath("//*[@id='dblClkBtn']"));
		
		Actions action = new Actions(driver);
		
		action.doubleClick(doubleClickElement).build().perform();

		Alert alert = driver.switchTo().alert();

		alert.accept();

		Thread.sleep(5000);
		
		driver.close();

	}



}

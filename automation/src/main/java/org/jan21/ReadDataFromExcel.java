package org.jan21;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadDataFromExcel {

	public static void main(String[] args) throws IOException {

		FileInputStream fis = new FileInputStream("C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\Book1.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fis);
		// HSSFWorkbook wb= new HSSFWorkbook(fis) for xls

		XSSFSheet sheet = wb.getSheetAt(0);

		XSSFRow firstRow = sheet.getRow(0);

		XSSFCell firstCell = firstRow.getCell(0);

		XSSFCell secondCell = firstRow.getCell(1);

		System.out.println(firstCell);

		System.out.println(secondCell);

	}

	public static String getExcelData(String sheetName, int rowNo, int cellNo) throws IOException {

		FileInputStream fis = new FileInputStream("C:\\Users\\Ankush\\git\\trainingjanuary\\automation\\Book1.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fis);
		// HSSFWorkbook wb= new HSSFWorkbook(fis) for xls

		// XSSFSheet sheet=wb.getSheetAt(0);

		XSSFSheet sheet = wb.getSheet(sheetName);

		XSSFRow row = sheet.getRow(rowNo);

		XSSFCell cell = row.getCell(cellNo);

		return cell.getStringCellValue();

	}

}
